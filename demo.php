<?php

include 'vendor/autoload.php';

use Flowapp\Intbot\Intbot;

// Create Customers

// 1. load flow
Intbot::load('nLEsw9Nested');
$customerMasterFlow = Intbot::flow('nLEsw9Nested');

echo '<pre>';
print_r($customerMasterFlow->getRecords([
  'paginate' => [
    'page' => 1,
    'per_page' => 10
  ]
]));
echo '</pre>';
